package com.siddharth.core.example1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		
		Student student = (Student) context.getBean("std");
		System.out.println(student);
		System.out.println("Hello world");

	}

}
